<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Misc
 */
$GLOBALS['TL_LANG']['avisota_send_immediate']['headline']     = 'Send message now';
$GLOBALS['TL_LANG']['avisota_send_immediate']['help']         = 'Send this message to %d recipients immediately.';
$GLOBALS['TL_LANG']['avisota_send_immediate']['confirmation'] = 'Are you sure you wan\'t to send this newsletter now? The sending process will start immediately!';
$GLOBALS['TL_LANG']['avisota_send_immediate']['action']       = 'Send message to recipients now';
$GLOBALS['TL_LANG']['avisota_send_immediate']['message']      = 'Enqueued %d messages int turn %d.';
