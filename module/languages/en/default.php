<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Mailing content elements
 */
$GLOBALS['TL_LANG']['MCE']['texts']     = 'Text elements';
$GLOBALS['TL_LANG']['MCE']['links']     = 'Link elements';
$GLOBALS['TL_LANG']['MCE']['images']    = 'Image elements';
$GLOBALS['TL_LANG']['MCE']['files']     = 'File elements';
$GLOBALS['TL_LANG']['MCE']['includes']  = 'Include elements';
