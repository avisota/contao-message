<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Module
 */
$GLOBALS['TL_LANG']['MOD']['avisota-message'] = array(
	'Avisota - Message',
	'Message management and editing for Avisota.'
);

/**
 * Frontend modules
 */
$GLOBALS['TL_LANG']['FMD']['avisota_message_list'] = array(
	'Message list',
	'Adds a list of newsletters to the page.'
);
$GLOBALS['TL_LANG']['FMD']['avisota_message_reader'] = array(
	'Message reader',
	'Shows the details of a newsletter.'
);
