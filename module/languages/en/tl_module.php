<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['avisota_message_categories'] = array(
	'Categories',
	'Please select one or more categories.'
);
$GLOBALS['TL_LANG']['tl_module']['avisota_message_layout']     = array(
	'Layout',
	'Please choose the layout to render the message.'
);
$GLOBALS['TL_LANG']['tl_module']['avisota_message_cell']       = array(
	'Cell',
	'Please choose the cell to render.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_module']['avisota_message_list_legend']   = 'List settings';
$GLOBALS['TL_LANG']['tl_module']['avisota_message_reader_legend'] = 'Reader settings';
